var mongoose = require('mongoose');

module.exports = function(){

  var callSchema = mongoose.Schema({

    tipo      : {type: String, trim: true},
    estado    : {type: String, trim: true},
    motivo    : {type: String, trim: true},
    descricao : {type: String, trim: true},
    created   : {type: Date, default: Date.now}

  });

  return mongoose.model('Call', callSchema);

}
