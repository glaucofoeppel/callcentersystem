# Call Center System #

Simples sistema de registro de chamados para call centers escrito em Node.js com framework Express.js.

### Como configurar ###

* Faça a instalação do Node.js ([https://nodejs.org/en/](Link URL)) 
* Faça a instalação do MongoDb ([https://www.mongodb.com/](Link URL))
* Faça o clone do projeto
* Ajuste a configuração do MongoDb alterando o arquivo **app.js** na raiz do projeto.
* Pelo terminal, acesse a raiz do projeto e rode o comando: **npm install** para instalar todas as dependências necessárias para o projeto
* Não existe migrações de banco para esse projeto, a configuração esta integrada ao sistema
* Inicie a aplicação utilizando o comando: **npm start**
* Inicie os testes da aplicação utilizando o comando: **npm test**
* Com os testes ok, acesse no browser o endereço: http://localhost:3000