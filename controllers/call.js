var moment = require("moment");

module.exports = function(app){

  var Call = app.models.call;

  var CallController = {

    /*
     * Method description: Render the page of register
     */
    index: function(req, res){
      res.render('call/index');
    },

    /*
     * Method description: Register the data if all the fields are selected
     */
    registrar: function(req, res){

      var erro = 0;

      if(req.body.tipo == ""){ erro = 1; }
      if(req.body.estado == ""){ erro = 1; }
      if(req.body.motivo == ""){ erro = 1; }
      if(req.body.descricao == ""){ erro = 1; }

      if(erro > 0){
        req.flash('erro', 'Todos os campos são para preenchimento obrigatório.');
        res.redirect('/');
      }else{
        var call = new Call();
        call.tipo = req.body.tipo;
        call.estado = req.body.estado;
        call.motivo = req.body.motivo;
        call.descricao = req.body.descricao;
        call.save(function(err){
          if(err){
            req.flash('erro', 'Erro ao gravar os campos.');
            res.redirect('/');
          }else{
            req.flash('success', 'Dados gravados com sucesso.');
            res.redirect('/');
          }
        });
      }

    },

    /*
     * Method: List all records
     */
    listar: function(req, res){

      Call.aggregate([
        {
            "$project":{
                "date": { "$dateToString": { format: "%d/%m/%Y", date: "$created"}},
                "tipo": "$tipo",
                "estado": "$estado",
                "motivo": "$motivo",
                "descricao": "$descricao"
            }
        },
        {
          "$sort": { date: 1, estado: 1 }
        },
        {
          "$group": {
            "_id": "$date",
            "item": { "$push": {
                          tipo: "$tipo",
                          estado: "$estado",
                          motivo: "$motivo",
                          descricao: "$descricao"
                        }
                    }
          }
        }
      ], function(err, data){
          res.render("call/listar", {data: data});
      });
    }
  }

  return CallController;

};
