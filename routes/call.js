module.exports = function(app){

  var call = app.controllers.call;

  app.route("/").get(call.index);
  app.route("/registrar").post(call.registrar);
  app.route("/listar").get(call.listar);
};
