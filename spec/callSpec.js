var request   = require("request");
var selenium  = require("selenium-webdriver");

var host = "http://localhost:3000/";

describe("Test Call Center System", function(){

  beforeEach(function(done) {
      this.driver = new selenium.Builder().
          withCapabilities(selenium.Capabilities.chrome()).
          build();

      this.driver.get('http://localhost:3000/').then(done);
  });

  afterEach(function(done) {
      this.driver.quit().then(done);
  });


  it("Should not register when 'tipo' field is empty", function(done){

      var searchForm  = this.driver.findElement(selenium.By.tagName('form'));

      var estado      = searchForm.findElement(selenium.By.name('estado'));
      var motivo      = searchForm.findElement(selenium.By.name('motivo'));
      var descricao   = searchForm.findElement(selenium.By.name('descricao'));

      estado.sendKeys("SP");
      motivo.sendKeys("elogio");
      descricao.sendKeys("Teste");

      searchForm.submit();

      this.driver.findElement(selenium.By.id('msgError')).getText().then(function(msg){
        expect(msg).toBe("Todos os campos são para preenchimento obrigatório.");
        done();
      });
  });

  it("Should not register when 'estado' field is empty", function(done){

      var searchForm  = this.driver.findElement(selenium.By.tagName('form'));

      var tipo        = searchForm.findElement(selenium.By.name('tipo'));
      var motivo      = searchForm.findElement(selenium.By.name('motivo'));
      var descricao   = searchForm.findElement(selenium.By.name('descricao'));

      tipo.sendKeys("telefone");
      motivo.sendKeys("elogio");
      descricao.sendKeys("Teste");

      searchForm.submit();

      this.driver.findElement(selenium.By.id('msgError')).getText().then(function(msg){
        expect(msg).toBe("Todos os campos são para preenchimento obrigatório.");
        done();
      });
  });

  it("Should not register when 'motivo' field is empty", function(done){

      var searchForm  = this.driver.findElement(selenium.By.tagName('form'));

      var tipo        = searchForm.findElement(selenium.By.name('tipo'));
      var estado      = searchForm.findElement(selenium.By.name('estado'));
      var descricao   = searchForm.findElement(selenium.By.name('descricao'));

      tipo.sendKeys("telefone");
      estado.sendKeys("SP");
      descricao.sendKeys("Teste");

      searchForm.submit();

      this.driver.findElement(selenium.By.id('msgError')).getText().then(function(msg){
        expect(msg).toBe("Todos os campos são para preenchimento obrigatório.");
        done();
      });
  });

  it("Should not register when 'descricao' field is empty", function(done){

      var searchForm  = this.driver.findElement(selenium.By.tagName('form'));

      var tipo        = searchForm.findElement(selenium.By.name('tipo'));
      var estado      = searchForm.findElement(selenium.By.name('estado'));
      var motivo      = searchForm.findElement(selenium.By.name('motivo'));

      tipo.sendKeys("telefone");
      estado.sendKeys("SP");
      motivo.sendKeys("elogio");

      searchForm.submit();

      this.driver.findElement(selenium.By.id('msgError')).getText().then(function(msg){
        expect(msg).toBe("Todos os campos são para preenchimento obrigatório.");
        done();
      });
  });

  it("Should register when all fields form is not empty", function(done){

      var searchForm  = this.driver.findElement(selenium.By.tagName('form'));

      var tipo      = searchForm.findElement(selenium.By.name('tipo'));
      var estado      = searchForm.findElement(selenium.By.name('estado'));
      var motivo      = searchForm.findElement(selenium.By.name('motivo'));
      var descricao   = searchForm.findElement(selenium.By.name('descricao'));

      tipo.sendKeys("telefone");
      estado.sendKeys("SP");
      motivo.sendKeys("elogio");
      descricao.sendKeys("Teste");

      searchForm.submit();

      this.driver.findElement(selenium.By.id('msgSuccess')).getText().then(function(msg){
        expect(msg).toBe("Dados gravados com sucesso.");
        done();
      });
  });

});
